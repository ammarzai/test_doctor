<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersClinicTiminig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('user_clinic_timing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('clinic_id');
            $table->string('day',100);
            $table->string('open_time',100);
            $table->string('close_time',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification');
    }
}
