<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserClinic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('users_clinic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_is');
            $table->string('clinic_name');
            $table->string('clinic_email');
            $table->string('clinic_phone');
             $table->string('clinic_discription');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_clinic');
    }
}
