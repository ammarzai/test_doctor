
<!DOCTYPE html>
<html dir="ltr" lang="en-US" class="no-js" ng-app="App">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="robots" content="index, follow">
        <title></title>

        <!-- Meta Description -->
        <meta name="description" content="jfghdfjgh">

        <!-- For Mobile Meta -->
        <meta name="HandheldFriendly" content="true">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
       
        <!-- Favicon -->
        <link rel="icon" type="image/png" sizes="32x32" href="//192.168.2.103:3000/images/favicon/favicon-32x32.png">
        <link rel="apple-touch-icon" sizes="180x180" href="//192.168.2.103:3000/images/favicon/apple-icon-180x180.png">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

        <link href="css/admin.css" rel="stylesheet">

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    </head>
    
    <body>

        <div class="site-wrapper">
                        <!-- Content Part -->
            @yield('content')
            <!-- Content Part End -->
        </div>
        <!-- Main Wrapper Ends -->

    </body>
   
</html>