
<!DOCTYPE html>
<html dir="ltr" lang="en-US" class="no-js" ng-app="App">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="robots" content="index, follow">
        <title></title>

        <!-- Meta Description -->
        <meta name="description" content="jfghdfjgh">

        <!-- For Mobile Meta -->
        <meta name="HandheldFriendly" content="true">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
       
        <!-- Favicon -->
        <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-icon-180x180.png')}}">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
        
        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    </head>
    
    <body data-is-fixed="false">

        <div class="site-wrapper">

            <div class="dashboard-content">
                <!-- User Header -->
                <!-- Dashboard Header -->
                <nav class="col navbar navbar-expand-lg bg-white shadow header">
                    <div class="collapse navbar-collapse [ d-flex justify-content-between ]" id="navbarNavDropdown">
                        <div class="col-sm-3 [ pl-0 ]">
                            <img class="company-logo" src="{{ asset('images/company_placeholder.png') }}" alt="">
                        </div>
                        
                        <ul class="user-dropdown navbar-nav col-sm-4 col-lg-3 col-xl-4 [ pr-0 align-items-center  justify-content-end ]">
                            <li class="nav-item">
                                <span>
                                    <strong class="text-primary font-weight-semibold font-normal [ d-block text-capitalize text-right ]">John Doe</strong>
                                        <small class="d-block text-muted text-capitalize">ADMIN &middot; VETERINARIAN</small>
                                </span>
                            </li>
                            <li class="nav-item dropdown [ ml-2 ]" >
                                <div class="nav-link dropdown-toggle [ pr-0 ] no-arrow" id="single-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <div class="row no-gutters [ align-items-center ]">
                                        <div class="user-img col-auto">
                                            <img class="rounded-circle" src="{{ asset('images/ic_placeholder.png')}}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-menu user-option" aria-labelledby="single-button">
                                    <a class="dropdown-item logout-user" value="Logout" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        <i class="dropdown-item--icon"></i>
                                        <span class="pl-3 align-middle text-red">
                                            Logout                            
                                        </span>
                                    </a>
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav><!-- Dashboard Header Ends -->

                 @yield('content')
            </div>
        </div>  
        <!-- Main Wrapper Ends -->



<!-- Modal -->
<div class="modal fade" id="addClinicModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-large" role="document">
    <form  id="clinic">
           {{ csrf_field() }}

    <div class="modal-content">
     <!--  <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        
        <div class="box">
          <div class="row">
            <h5 class="box__heading col-xl-12">Clinic Details</h5>
            <div class="form-group col-sm-6 col-lg-6 col-xl-4">
              <div class="input-section">
                <input type="text" name="clinicName" class="form-control" id="clinic-name" placeholder="Enter clinic name" value="" required>
                <label for="clinic-name">Clinic Name*</label>

                <!-- <div class="form-error">
                   <span>Company name is required.</span>
                </div>  -->
              </div>                      
            </div>
            <div class="form-group col-sm-6 col-lg-6 col-xl-4">
              <div class="input-section">
                <input type="text" name="clinicEmail" class="form-control" id="clinic-email" placeholder="Enter clinic email" required>
                <label for="clinic-email">Clinic Email*</label>

                <!-- <div class="form-error">
                   <span>Company email is required.</span>
                </div>  -->
              </div>                       
            </div>
            <div class="form-group col-sm-6 col-lg-6 col-xl-4">
              <div class="input-section">
                <input type="text" name="companyPhone" class="form-control" id="clinic-phone"  placeholder="Enter clinic phone" value="">
                <label for="clinic-phone">Clinic Phone*</label>
                <!-- <div class="form-error">
                   <span>Company phone is required.</span>
                </div>  -->
              </div>                      
            </div>
          </div>    
          <div class="row">
            <div class="form-group col">
              <div class="input-section">
                <textarea name="clinic_discription" id="about-clinic" class="form-control " placeholder="Enter about clinic"></textarea>
                <label for="about-clinic">About Clinic</label> 
              </div>    
            </div>
          </div>
        </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
        <button type="button" id="clinicadd" class="btn btn-primary">Save</button>
      </div>
    </div>
</form>
  </div>
</div>


        <script type="text/javascript">
            $('document').ready(function () {
                var distance = $('.page-heading').offset().top,
                        $window = $(window);

                $window.scroll(function () {

                    if ($window.scrollTop() >= distance) {

                        $('body').attr("data-is-fixed", "true");

                    } else {

                        $('body').attr("data-is-fixed", "false");

                    }

                    $('.acuro-sidebar').css('top', 114 - $(this).scrollTop());

                });


                $('#js-notification-trigger').on("click", function () {
                    $('.user-notification-sidebar').toggleClass('active');
                });



$("#submit").on("click", function(e) {
    e.preventDefault();
    

        var file_data = $('#pro_pic').prop('files')[0];
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var licence_number = $('#licence_no').val();



        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('first_name', first_name);
        form_data.append('last_name', last_name);
        form_data.append('email', email);
        form_data.append('phone', phone);
        form_data.append('licence_number', licence_number);
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

        $.ajax({
            url: "{{url('users/edits')}}", // point to server-side PHP script
            data: form_data,
            type: 'POST',
            contentType: false, // The content type used when sending data to the server.
            cache: false, // To unable request pages to be cached
            processData: false,
            success: function(data) {

            }
        });
    
});


$("#clinicadd").on("click", function(e) {

    e.preventDefault();
     var formData = {
                clinic_discription     :$('textarea[name=clinic_discription]').val(),
                clinicName    : $('input[name=clinicName]').val(),
                clinicEmail : $('input[name=clinicEmail]').val(),
                companyPhone  : $('input[name=companyPhone]').val()
            }
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

        $.ajax({
            url: "{{url('clinic/add')}}", // point to server-side PHP script
            type     : "POST",
                // url      : $(this).attr('action') + '/store',
               
                data     : formData,
                cache    : false,
            success: function(data) {

            }
        });
    
});


            });
function openclose(oForm,id,ev){

ev.preventDefault();



        $.ajax({
            url: "{{url('clinic/openclose')}}", // point to server-side PHP script
            type     : "POST",
                // url      : $(this).attr('action') + '/store',
               
                data     : $( oForm ).serialize(),
                cache    : false,
            success: function(data) {

            }
        });

}
function deletetime(id,cid,ev){

ev.preventDefault();
  var formData = {
                id     :id,
                clinicId    : cid
            }
   $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });

        $.ajax({
            url: "{{url('clinic/deletetime')}}", // point to server-side PHP script
            type     : "POST",
                // url      : $(this).attr('action') + '/store',
               
                data     : formData,
                cache    : false,
            success: function(data) {

            }
        });

}

$(function () {
            $('.timeformatExample').timepicker({
               timeFormat: 'HH:mm',
               
                dynamic: true,
                dropdown: true,
                scrollbar: true
            });
        });   
        </script>

    </body>
   
</html>