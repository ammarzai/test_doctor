@extends('layouts.frontend')

@section('content')
 <div class="sign-in-wrapper mt-5">
           
                <form class="sign-in__form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                <div class="sign-in radius">
                    <div class="row [ d-flex mb-5 ]">
                        <h1 class="sign-in__heading col-md-6 [ mb-0 ]">Sign Up</h1>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-section">
                                <input type="text" class="form-control" placeholder="Enter first name"
                                       name="first_name" id="first_name"
                                       required>
                                <label for="first_name">First Name*</label>

                               <!--  <div class="form-error">
                                    <span>First name is required.</span>
                                </div> --> <!-- /.form-error -->
                            </div>
                            @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-section">
                                <input type="text" class="form-control" placeholder="Enter last name"
                                       name="last_name" id="last_name"
                                       required>
                                <label for="last_name">Last Name*</label>
                                <!-- <div class="form-error"">
                                    <span>Last name is required.</span>
                                </div> --> <!-- /.form-error -->
                            </div>
                            @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="input-section">
                                <input type="text" class="form-control" placeholder="Enter email"
                                       name="email" id="email"
                                       required value="{{ old('email') }}">
                                <label for="email">Email*</label>
                                <!-- <div class="form-error">
                                    <span>Email is required.</span>
                                    <span>Email is invalid.</span>
                                </div> --> <!-- /.form-error -->
                            </div>
                            @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group col-md-6">
                            <div class="input-section">
                                <input type="text" class="form-control" placeholder="Enter phone number"
                                       name="phone" id="phone" required>
                                <label for="phone">Phone*</label>
                                <!-- <div class="form-error">
                                    <span>Phone number is required.</span>
                                    <span>Phone number is invalid.</span>
                                </div> --> <!-- /.form-error -->
                            </div>
                             @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 [ mb-0 ]">
                            <div class="input-section">
                                <input type="password" class="form-control" placeholder="Enter password"
                                       name="password" id="password"
                                       required>
                                <label for="password">Password*</label>
                                <!-- <div class="form-error">
                                    <span>Password is required.</span>
                                </div> --> <!-- /.form-error -->
                            </div>
                            @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group col-md-6 [ mb-0 ]">
                            <label id="term-condition-label" class="custom-control custom-checkbox [ font-weight-bold ]"
                                   for="term-condition">
                                <input type="checkbox" class="custom-control-input" id="term-condition" name="terms" value="1" required>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description agreement-text [ pl-2 ]">I agree</span>
                            </label>
                            <!-- <div class="form-error [ pl-3 ]" >
                                <span>Accept terms and conditions.</span>
                            </div> --> <!-- /.form-error -->
                        </div>
                    </div>
                </div> <!-- /.sign-in -->
                <button class="btn btn-primary btn-large sign-in__btn [ mx-auto ]" id="sign-in__btn"> Sign Up </button>
            </form> <!-- /.sign-in__form -->
        </div>
@endsection
