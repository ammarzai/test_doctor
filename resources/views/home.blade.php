@extends('layouts.app')

@section('content')
<div>
                    <!-- Module Heading Bar -->
                    <div class="page-heading shadow [ d-flex flex-sm-wrap align-items-center  justify-content-start fix-width-heading ]">
                        <h5 class="primary-text-dark col-sm-3 font-large [ mb-0 text-capitalize pl-0 ]  ">
                            Edit Profile 
                        </h5>
                    </div>    

                    <div>
                        <!-- Admin setting sidebar menu -->
                        <aside class="acuro-sidebar acuro-sidebar--large">
                            <form method="" id="form" enctype="multipart/form-data">
                                <meta name="_token" content="{{ csrf_token() }}" />
                            <div class="box">
                                <div class="d-flex justify-content-center mb-5">
                                    <div class="user-profile-dp">
                                        <img src="{{ asset('images/ic_placeholder.png')}}" id="profile_img" alt="">
                                        <span role="button" class="user-profile-dp__edit" >
                                            <img src="{{ asset('images/ic_edit.png')}}" alt="">
                                            <input type="file" name="pro_pic" id="pro_pic">
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col">
                                        <div class="input-section">
                                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First name" value="{{$data->first_name}}" required>
                                           <!--  <div class="form-error">
                                                <span>First name is required.</span>
                                            </div>  --> 
                                            <label>First Name*</label>
                                        </div>                      
                                    </div>       
                                </div>   
                                <div class="row">
                                    <div class="form-group col">
                                        <div class="input-section">
                                            <input type="text" class="form-control" id="last_name" placeholder="Last name" name="last_name"  required value="{{$data->last_name}}">
                                            <!-- <div class="form-error" >
                                                <span ng-message="required">Last name is required.</span>
                                            </div> -->   
                                            <label>Last Name*</label>
                                        </div>                      
                                    </div>       
                                </div>   
                                <div class="row">
                                    <div class="form-group col">
                                        <div class="input-section">
                                            <input type="text" class="form-control" id="email" placeholder="Enter email" name="email" required value="{{$data->email}}">
                                            <!-- <div class="form-error" >
                                                <span ng-message="required">Email is required.</span>
                                            </div> -->
                                            <label for="email">Email*</label>                                
                                        </div>                      
                                    </div>       
                                </div>   
                                <div class="row">
                                    <div class="form-group col">
                                        <div class="input-section">
                                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Enter your phone" name="phone" required value="{{$data->phone}}">
                                            <!-- <div class="form-error" >
                                                <span ng-message="required">Phone is required.</span>
                                            </div> -->
                                            <label for="phone">Phone*</label>            
                                        </div>                      
                                    </div>         
                                </div>   
                                <div class="row">
                                    <div class="form-group col">
                                        <div class="input-section">
                                            <input type="text" class="form-control" id="licence_no" placeholder="Enter license number" name="licence_number" required value="{{$data->licence_number}}">
                                            <label for="licence_no">License Number</label>
                                           <!--  <div class="form-error" >
                                                <span>Licence number is already exist.</span>
                                            </div> -->
                                        </div>                      
                                    </div>         
                                </div>
                            </div>
                        </form>
                        </aside>
                        <!-- Admin setting sidebar menu Ends-->
                        <!-- Main area -->
                        <div class="main-content">
                            <div class="box-wrapper">

                                <div class="box">
                                    <div class="d-flex justify-content-between align-items-center mb-4">
                                        <h4 class="box__heading [ mb-0 ]">Clinics and Timings</h4>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addClinicModal">Add Clinic</button>
                                    </div>

                                    <div id="accordion-parent">
                                        @foreach($clinincs as $key => $clininc)
                                         <div id="accordion{{$clininc->id}}" role="tablist" data-accordion-theme="white" data-toggle-icon="arrow" data-accordion-style="border" class="card p-0 border  mb-2">
                                            <div class="" role="tab" id="heading{{$clininc->id}}">
                                                <h5 class="mb-0">
                                                <a data-toggle="collapse" href="#collapse{{$clininc->id}}" role="button"  aria-controls="collapse{{$clininc->id}}" data-toggle="collapse" data-target="#collapse{{$clininc->id}}">
                                                  <div class="accordion__heading height-auto">
                                                    <div class="py-2">
                                                        <h6 class="primary-text mb-2"> Clinic 1</h6>
                                                        <span class="font-normal font-weight-normal text-muted d-inline-block position-relative pl-4 mr-4">
                                                            <i class="ic-phone"></i>  
                                                            {{$clininc->clinic_phone}}
                                                        </span>
                                                        <span class="font-normal font-weight-normal text-muted d-inline-block position-relative pl-4">
                                                            <i class="ic-email"></i>  
                                                            {{$clininc->clinic_email}}
                                                        </span>
                                                    </div>
                                                   <i class="toggle-icon">
                                                   </i>
                                                </div>
                                                </a>
                                                </h5>
                                            </div><!-- end accordion heading-->

                                            <div id="collapse{{$clininc->id}}" class="collapse divider-top" role="tabpanel" aria-labelledby="heading{{$clininc->id}}" data-parent="#accordion-parent">
                                              <div class="card-body p-0">
                                               <div>
                                                    <div class="box [ mb-0 pt-4 pb-0 ] position-relative z-index-0" >
                                                        <form action="#" id="openclose">
                                                              {{ csrf_field() }}
                                                              <input type="hidden" name="clinic_id" value="{{$clininc->id}}">
                                                        <div class="row [ mb-4 ]">
                                                            <div class="col-auto [ d-flex align-items-center ]">
                                                                <div class="btn-group checkbox-group" data-toggle="buttons">
                                                                    <label class="btn" >
                                                                        <input type="checkbox" autocomplete="off" name="mon"> Mon
                                                                    </label>
                                                                    <label class="btn">
                                                                        <input type="checkbox" autocomplete="off" name="tue"> Tue
                                                                    </label>
                                                                    <label class="btn" >
                                                                        <input type="checkbox"  autocomplete="off" name="wed"> Wed
                                                                    </label>
                                                                    <label class="btn" >
                                                                        <input type="checkbox" autocomplete="off" name="thu"> Thu
                                                                    </label>
                                                                    <label class="btn" >
                                                                        <input type="checkbox"  autocomplete="off" name="fri"> Fri
                                                                    </label>
                                                                    <label class="btn" >
                                                                        <input type="checkbox" autocomplete="off" name="sat"> Sat
                                                                    </label>
                                                                    <label class="btn">
                                                                        <input type="checkbox" autocomplete="off" name="sun"> Sun
                                                                    </label>
                                                                    <div class="form-error" >
                                                                        <span></span>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                            <div class="col-auto [ pl-lg-0 pr-lg-0 pl-xl-3 pr-xl-3 ]">
                                                                <div class="time-group">
                                                                    <div class="time-group__item">
                                                                        <input type="text" class="form-control time-group__input timeformatExample" placeholder="Start" name="start" id="timeformatExample1" />
                                                                    </div>
                                                                    <span class="mx-3">
                                                                        To
                                                                    </span>
                                                                    <div class="time-group__item">
                                                                        <input type="text" class="form-control time-group__input timeformatExample" placeholder="End" name="end"/>
                                                                    </div>
                                                                </div>
                                                                <div class="form-error form-error--full">
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-auto [ pr-0 d-flex align-items-center ]">
                                                                <button id="timeadd" type="btn" class="btn btn-add" onclick="openclose(this.form,'{{$clininc->id}}',event)"></button>
                                                            </div>
                                                        </div>
                                                    </form>

                                                        <div class="appointment-list divider-top">
                                                            @php
                                                             $a=json_encode($clininc->clinictiming);
                                                    
                                                         

                                                            $ctime=json_decode($a);
                                                            $time=array();
                                                            foreach($ctime as $ctimes){
                                                          
                                                            $time[$ctimes->day]=$ctimes;
                                                        }
                                                        

                                                            @endphp
                                                            
                                                          <div class="appointment-list__item">
                                                                <div class="appointment-list__day">
                                                                    Monday
                                                                </div>
                                                                <div class="appointment-list__time">
                                                                    <div class="badge badge-pill bg-light d-flex align-items-center secondary-text-dark ng-scope" >
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                        @php
                                                                        if (array_key_exists("mon",$time)){
                                                                           echo date('h:i A', strtotime($time['mon']->open_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                        <span class="font-small px-2 text-muted">
                                                                            To
                                                                        </span>
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                             @php
                                                                        if (array_key_exists("mon",$time)){
                                                                           echo date('h:i A', strtotime($time['mon']->close_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                         @php
                                                                        if (array_key_exists("mon",$time)){ @endphp
                                                                          <a class="ic-close ml-2" onclick="deletetime('{{$time['mon']->id}}','{{$clininc->id}}',event)">
                                                                        </a>
                                                                       @php } @endphp
                               
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="appointment-list__item">
                                                            <div class="appointment-list__day">
                                                                    Tuesday
                                                                </div>
                                                                <div class="appointment-list__time">
                                                                    <div class="badge badge-pill bg-light d-flex align-items-center secondary-text-dark ng-scope" >
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                        @php
                                                                        if (array_key_exists("tue",$time)){
                                                                           echo date('h:i A', strtotime($time['tue']->open_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                        <span class="font-small px-2 text-muted">
                                                                            To
                                                                        </span>
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                               @php
                                                                        if (array_key_exists("tue",$time)){
                                                                           echo date('h:i A', strtotime($time['tue']->close_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                        @php
                                                                        if (array_key_exists("tue",$time)){ @endphp
                                                                          <a class="ic-close ml-2" onclick="deletetime('{{$time['tue']->id}}','{{$clininc->id}}',event)">
                                                                        </a>
                                                                       @php } @endphp
                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="appointment-list__item">
                                                            <div class="appointment-list__day">
                                                                    Wednesday
                                                                </div>
                                                                <div class="appointment-list__time">
                                                                    <div class="badge badge-pill bg-light d-flex align-items-center secondary-text-dark ng-scope" >
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                        @php
                                                                        if (array_key_exists("wed",$time)){
                                                                           echo date('h:i A', strtotime($time['wed']->open_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                        <span class="font-small px-2 text-muted">
                                                                            To
                                                                        </span>
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                              @php
                                                                        if (array_key_exists("wed",$time)){
                                                                           echo date('h:i A', strtotime($time['wed']->close_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                       </span>
                                                                        @php
                                                                        if (array_key_exists("wed",$time)){ @endphp
                                                                          <a class="ic-close ml-2" onclick="deletetime('{{$time['wed']->id}}','{{$clininc->id}}',event)">
                                                                        </a>
                                                                       @php } @endphp
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="appointment-list__item">
                                                            <div class="appointment-list__day">
                                                                    Thursday
                                                                </div>
                                                                <div class="appointment-list__time">
                                                                    <div class="badge badge-pill bg-light d-flex align-items-center secondary-text-dark ng-scope" >
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                        @php
                                                                        if (array_key_exists("thu",$time)){
                                                                           echo date('h:i A', strtotime($time['thu']->open_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                        <span class="font-small px-2 text-muted">
                                                                            To
                                                                        </span>
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                             @php
                                                                        if (array_key_exists("thu",$time)){
                                                                           echo date('h:i A', strtotime($time['thu']->close_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                        @php
                                                                        if (array_key_exists("thu",$time)){ @endphp
                                                                          <a class="ic-close ml-2" onclick="deletetime('{{$time['thu']->id}}','{{$clininc->id}}',event)">
                                                                        </a>
                                                                       @php } @endphp
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="appointment-list__item">
                                                            <div class="appointment-list__day">
                                                                    Friday
                                                                </div>
                                                                <div class="appointment-list__time">
                                                                    <div class="badge badge-pill bg-light d-flex align-items-center secondary-text-dark ng-scope" >
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                        @php
                                                                        if (array_key_exists("fri",$time)){
                                                                           echo date('h:i A', strtotime($time['fri']->open_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                        <span class="font-small px-2 text-muted">
                                                                            To
                                                                        </span>
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                             @php
                                                                        if (array_key_exists("fri",$time)){
                                                                           echo date('h:i A', strtotime($time['fri']->close_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                          @php
                                                                        if (array_key_exists("fri",$time)){ @endphp
                                                                          <a class="ic-close ml-2" onclick="deletetime('{{$time['fri']->id}}','{{$clininc->id}}',event)">
                                                                        </a>
                                                                       @php } @endphp
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="appointment-list__item">
                                                            <div class="appointment-list__day">
                                                                    Saturday
                                                                </div>
                                                                <div class="appointment-list__time">
                                                                    <div class="badge badge-pill bg-light d-flex align-items-center secondary-text-dark ng-scope" >
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                        @php
                                                                        if (array_key_exists("sat",$time)){
                                                                           echo date('h:i A', strtotime($time['sat']->open_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                        <span class="font-small px-2 text-muted">
                                                                            To
                                                                        </span>
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                             @php
                                                                        if (array_key_exists("sat",$time)){
                                                                           echo date('h:i A', strtotime($time['sat']->close_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                          @php
                                                                        if (array_key_exists("sat",$time)){ @endphp
                                                                          <a class="ic-close ml-2" onclick="deletetime('{{$time['sat']->id}}','{{$clininc->id}}',event)">
                                                                        </a>
                                                                       @php } @endphp
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="appointment-list__item">
                                                            <div class="appointment-list__day">
                                                                    Sunday
                                                                </div>
                                                                <div class="appointment-list__time">
                                                                    <div class="badge badge-pill bg-light d-flex align-items-center secondary-text-dark ng-scope" >
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                        @php
                                                                        if (array_key_exists("sun",$time)){
                                                                           echo date('h:i A', strtotime($time['sun']->open_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                        <span class="font-small px-2 text-muted">
                                                                            To
                                                                        </span>
                                                                        <span class="font-small font-weight-bold ng-binding">
                                                                           @php
                                                                        if (array_key_exists("sun",$time)){
                                                                           echo date('h:i A', strtotime($time['sun']->close_time));
                                                                       }else{
                                                                       echo "--";
                                                                   }
                                                                       @endphp
                                                                        </span>
                                                                         @php
                                                                        if (array_key_exists("sun",$time)){ @endphp
                                                                          <a class="ic-close ml-2" onclick="deletetime('{{$time['sun']->id}}','{{$clininc->id}}',event)">
                                                                        </a>
                                                                       @php } @endphp
                                                                    </div>
                                                                </div>
                                                            </div>
                                                          
                                                            <!-- /.appointment-list__item   -->
                                                        </div>
                                                    </div>  
                                                </div>
                                              </div>
                                            </div><!-- end accordion body -->
                                        </div><!-- end accordion -->

                                        @endforeach

                                       
                                      
                                       
                                    </div>
                                </div>            

                                <div class="box">
                                    <div class="row">
                                        <h5 class="box__heading col-xl-12">About User</h5>
                                        <div class="col-sm-12">
                                            <div class="input-section">
                                                <textarea class="form-control" id="about" placeholder="Enter text here" name="about"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="box p-0">
                                    <div class="box mb-0 box-bottom--shadow" >
                                        <div class="row">
                                            <h5 class="box__heading col-xl-12">User Experience</h5>
                                            <div class="form-group col-sm-4">
                                                <div class="input-section">
                                                    <select class="custom-select w-100" id="role" name="role" >
                                                        <option selected>Select</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                    <label for="role">Role</label>
                                                </div>
                                            </div>
                                            <div class="form-group w-100p col-auto divider-date">
                                                <div class="input-section">
                                                    <input type="text" id="role_from_year" name="from_year" placeholder="yyyy" datepicker-mode="year" datetime-picker="yyyy" class="form-control roleFromYear icon-field ic-calendar"/>
                                                    <label for="role_from_year">From</label>
                                                </div>
                                            </div>
                                            <div class="form-group w-100p col-auto">
                                                <div class="input-section">
                                                    <input type="text" id="role_to_year" name="to_year" placeholder="yyyy" class="form-control roleToYear icon-field ic-calendar" />
                                                    <label for="role_to_year">To</label>
                                                </div>
                                            </div>
                                            <div class="form-group col">
                                                <div class="input-section">
                                                    <input type="text" id="clinic-hospital-companyname" name="company" placeholder="Type to add" class="form-control">
                                                    <label for="clinic-hospital-companyname">Clinic/Hospital/Company name</label>
                                                </div>
                                            </div>
                                            <div class="form-group col-auto">
                                                <button class="btn btn-add"></button>
                                            </div>
                                        </div>                           
                                    </div>

                                    <div class="table-container radius-none bg-transparent" >
                                        <div class="box [ py-0 mb-0 ]">
                                            <table class="table data-table box-table [ mb-0 ]">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <a href="#" class="sorting">Role
                                                            </a>
                                                        </th>
                                                        <th>
                                                            <a href="#" class="sorting">
                                                                Duration
                                                            </a>
                                                        </th>
                                                        <th>
                                                            <a href="#" class="sorting">Clinic/Hospital/Company name  
                                                            </a>
                                                        </th>
                                                        <th>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            Doctor
                                                        </td>
                                                        <td>
                                                            2000-2002
                                                        </td>
                                                        <td>
                                                            Abc Hospital
                                                        </td>
                                                        <td class="text-center p-0">
                                                            <a href="" class="ic-close btn-close  [ d-inline-block align-middle ml-1 ]">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Doctor
                                                        </td>
                                                        <td>
                                                            2000-2002
                                                        </td>
                                                        <td>
                                                            Abc Hospital
                                                        </td>
                                                        <td class="text-center p-0">
                                                            <a href="" class="ic-close btn-close  [ d-inline-block align-middle ml-1 ]">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div> 

                                <div class="row [ mt-4 ]">
                                    <div class="col-xl-12 [ text-right ]">
                                        <button type="button" id="submit" class="btn btn-primary">
                                            Save                            
                                        </button>
                                    </div>
                                </div>   

                            </div> <!-- /.box-wrapper -->
                        </div><!-- Main area Ends -->
                    </div>
                </div>
                <script>
                    
                </script>
@endsection
