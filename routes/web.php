<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::resource('users', 'UserController');
Route::get('/users/show', 'UserController@show');
Route::post('/users/edits', 'UserController@edits');
Route::post('/clinic/add', 'UsersClinicController@add');
Route::post('/clinic/openclose', 'UsersClinicController@openclose');
Route::post('/clinic/deletetime', 'UsersClinicController@deletetime');

