<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class UsersClinic extends Model
{
    //
    protected $table = 'users_clinic';
     protected $fillable = [
         'clinic_name','clinic_email','clinic_phone','clinic_discription','users_is',
    ];

    public function add($data){
    	
    	 $sqlData = array(
        'users_is'         => Auth::user()->id,
        'clinic_name'          => $data['clinicName'],
        'clinic_email'     => $data['clinicEmail'],
        'clinic_phone'      => $data['companyPhone'],
        'clinic_discription'       => $data['clinic_discription'],
    );
    	
    	UsersClinic::create($sqlData);
    }
    public function findAll($userid){
    	
    	 $clinics = UsersClinic::with('clinictiming')->where('users_is', $userid)
               ->orderBy('clinic_name', 'desc')
               ->get();
               return $clinics;
    }
     public function usersClinicOpencloseTime($id,$data){
     	$UsersClinic = UsersClinic::find($id);
     	if($UsersClinic) {
     		$UsersClinic->timing = $data;
     		$UsersClinic->save();
     	}
     }

      public function clinictiming()
    {
        return $this->hasMany('App\UsersClinicTiming','clinic_id','id');
    }
}
