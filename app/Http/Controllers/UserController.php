<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\UsersClinic;
use Auth;
use Illuminate\Support\Facades\Input;
class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->users = new  User;
        $this->usersClinic = new  UsersClinic;
        $this->middleware('auth',['except' => 'edits']);
    }

    public function index()
    {
        $id=Auth::user()->id;
        $data=$this->users->findById($id);
        
        return view('home',compact('data'));
    }
    public function show()
    {
        $id=Auth::user()->id;
        $data=$this->users->findById($id);
        $clinincs=$this->usersClinic->findAll($id);
/*echo"<pre>";
print_r($clinincs);
exit;*/
        return view('home',compact('data','clinincs'));
    }
        public function edits(Request $request)
    {
    	 $file = $request->file('file');
    	
    	
    	 if (Input::hasFile('file'))
{
    $destinationPath = 'uploads';
    	 echo $file->getClientOriginalName();
      $file->move($destinationPath,$file->getClientOriginalName());
      $filename=$file->getClientOriginalName();
}else{
	 $filename="";
}

    	   /* if($request->ajax()) {
    	    }
    	    $this->users->update($request$id=Auth::user()->id);
    	     $grocery = new User();
    	     $grocery->email = $request->email;
    	     $grocery->name = $request->name;
        $grocery->first_name = $request->first_name;
        $grocery->last_name = $request->last_name;
        $grocery->phone = $request->phone;*/
		User::find(Auth::user()->id)->update(['first_name' => $request->first_name, 'last_name' => $request->last_name, 'email' => $request->email, 'phone' => $request->phone, 'licence_number' => $request->licence_number, 'profile_pic' => $filename]); // Should ignore "evil_field" correct? In my case it doesn't

      //  $grocery->save();
        return response()->json(['success'=>'Data is successfully added']);
    }
}
