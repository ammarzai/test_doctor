<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\UsersClinic;
use App\UsersClinicTiming;
use Auth;
use Illuminate\Support\Facades\Input;

class UsersClinicController extends Controller
{
	public function __construct()
    {
        $this->usersClinic = new  UsersClinic;
        $this->UsersClinicTiming = new  UsersClinicTiming;
        $this->middleware('auth');
    }
     public function add(Request $request)
    {

    	$data=$request->all();
    	
    	$this->usersClinic->add($data);
      // echo  $id=Auth::user()->id;
       
    }
      public function openclose(Request $request)
    {

    	$data=$request->all();
    	
    	
    	 $week_days_open_close_time = array();
    	 $id=Auth::user()->id;

                 if (!empty($request->input('mon')) && $request->input('mon') == 'on') {

                 	$week_days_open_close_time["mon"]["open_time"]  = $request->input('start');
                 	$week_days_open_close_time["mon"]["close_time"] = $request->input('end');
				//	$week_days_open_close_time["mon"]["is_open"]    = 1;
					$week_days_open_close_time["mon"]["day"] = 'mon';
					$week_days_open_close_time["mon"]["clinic_id"] = $request->input('clinic_id');
					$week_days_open_close_time["mon"]["user_id"] = $id;
					$week_days_open_close_time["mon"]["created_at"] = date('Y-m-d H:i:s');
					$week_days_open_close_time["mon"]["updated_at"] = date('Y-m-d H:i:s');
				}/* else {
					$week_days_open_close_time["mon"]["open_time"]  = '';
					$week_days_open_close_time["mon"]["close_time"] = '';
					$week_days_open_close_time["mon"]["is_open"]    = 0;
				};*/
				if (!empty($request->input('tue')) && $request->input('tue') == 'on') {
                 	$week_days_open_close_time["tue"]["open_time"]  = $request->input('start');
                 	$week_days_open_close_time["tue"]["close_time"] = $request->input('end');
				//	$week_days_open_close_time["tue"]["is_open"]    = 1;
					$week_days_open_close_time["tue"]["day"] = 'tue';
					$week_days_open_close_time["tue"]["clinic_id"] = $request->input('clinic_id');
					$week_days_open_close_time["tue"]["user_id"] = $id;
					$week_days_open_close_time["tue"]["created_at"] = date('Y-m-d H:i:s');
					$week_days_open_close_time["tue"]["updated_at"] = date('Y-m-d H:i:s');
				} /*else {
					$week_days_open_close_time["tue"]["open_time"]  = '';
					$week_days_open_close_time["tue"]["close_time"] = '';
					$week_days_open_close_time["tue"]["is_open"]    = 0;
				};*/
				if (!empty($request->input('wed')) && $request->input('wed') == 'on') {
                 	$week_days_open_close_time["wed"]["open_time"]  = $request->input('start');
                 	$week_days_open_close_time["wed"]["close_time"] = $request->input('end');
				//	$week_days_open_close_time["wed"]["is_open"]    = 1;
					$week_days_open_close_time["wed"]["day"] = 'wed';
					$week_days_open_close_time["wed"]["clinic_id"] = $request->input('clinic_id');
					$week_days_open_close_time["wed"]["user_id"] = $id;
					$week_days_open_close_time["wed"]["created_at"] = date('Y-m-d H:i:s');
					$week_days_open_close_time["wed"]["updated_at"] = date('Y-m-d H:i:s');
				} /*else {
					$week_days_open_close_time["wed"]["open_time"]  = '';
					$week_days_open_close_time["wed"]["close_time"] = '';
					$week_days_open_close_time["wed"]["is_open"]    = 0;
				};*/
				if (!empty($request->input('thu')) && $request->input('thu') == 'on') {
                 	$week_days_open_close_time["thu"]["open_time"]  = $request->input('start');
                 	$week_days_open_close_time["thu"]["close_time"] = $request->input('end');
					//$week_days_open_close_time["thu"]["is_open"]    = 1;
					$week_days_open_close_time["thu"]["day"] = 'thu';
					$week_days_open_close_time["thu"]["clinic_id"] = $request->input('clinic_id');
					$week_days_open_close_time["thu"]["user_id"] = $id;
					$week_days_open_close_time["thu"]["created_at"] = date('Y-m-d H:i:s');
					$week_days_open_close_time["thu"]["updated_at"] = date('Y-m-d H:i:s');
				}/* else {
					$week_days_open_close_time["thu"]["open_time"]  = '';
					$week_days_open_close_time["thu"]["close_time"] = '';
					$week_days_open_close_time["thu"]["is_open"]    = 0;
				};*/
				if (!empty($request->input('fri')) && $request->input('sun') == 'on') {
                 	$week_days_open_close_time["fri"]["open_time"]  = $request->input('start');
                 	$week_days_open_close_time["fri"]["close_time"] = $request->input('end');
					//$week_days_open_close_time["fri"]["is_open"]    = 1;
					$week_days_open_close_time["fri"]["day"] = 'fri';
					$week_days_open_close_time["fri"]["clinic_id"] = $request->input('clinic_id');
					$week_days_open_close_time["fri"]["user_id"] = $id;
					$week_days_open_close_time["fri"]["created_at"] = date('Y-m-d H:i:s');
					$week_days_open_close_time["fri"]["updated_at"] = date('Y-m-d H:i:s');
				}/* else {
					$week_days_open_close_time["fri"]["open_time"]  = '';
					$week_days_open_close_time["fri"]["close_time"] = '';
					$week_days_open_close_time["fri"]["is_open"]    = 0;
				};*/
				if (!empty($request->input('sat')) && $request->input('sat') == 'on') {
                 	$week_days_open_close_time["sat"]["open_time"]  = $request->input('start');
                 	$week_days_open_close_time["sat"]["close_time"] = $request->input('end');
					//$week_days_open_close_time["sat"]["is_open"]    = 1;
					$week_days_open_close_time["sat"]["day"] = 'sat';
					$week_days_open_close_time["sat"]["clinic_id"] = $request->input('clinic_id');
					$week_days_open_close_time["sat"]["user_id"] = $id;
					$week_days_open_close_time["sat"]["created_at"] = date('Y-m-d H:i:s');
					$week_days_open_close_time["sat"]["updated_at"] = date('Y-m-d H:i:s');
				}/* else {
					$week_days_open_close_time["sat"]["open_time"]  = '';
					$week_days_open_close_time["sat"]["close_time"] = '';
					$week_days_open_close_time["sat"]["is_open"]    = 0;
				};*/
				if (!empty($request->input('sun')) && $request->input('sun') == 'on') {
                 	$week_days_open_close_time["sun"]["open_time"]  = $request->input('start');
                 	$week_days_open_close_time["sun"]["close_time"] = $request->input('end');
					//$week_days_open_close_time["sun"]["is_open"]    = 1;
					$week_days_open_close_time["sun"]["day"] = 'sun';
					$week_days_open_close_time["sun"]["clinic_id"] = $request->input('clinic_id');
					$week_days_open_close_time["sun"]["user_id"] = $id;
					$week_days_open_close_time["sun"]["created_at"] = date('Y-m-d H:i:s');
					$week_days_open_close_time["sun"]["updated_at"] = date('Y-m-d H:i:s');

				}/* else {
					$week_days_open_close_time["sun"]["open_time"]  = '';
					$week_days_open_close_time["sun"]["close_time"] = '';
					$week_days_open_close_time["sun"]["is_open"]    = 0;
				};*/
				
                  json_encode($week_days_open_close_time);

              
              //  $data=$request->all();
    	
    			$a=$this->UsersClinicTiming->usersClinicOpencloseTime($request->input('clinic_id'),$week_days_open_close_time);

    	exit;
    	//$this->usersClinic->add($data);
      // echo  $id=Auth::user()->id;
       
    }
      public function deletetime(Request $request){
      	
      	$this->UsersClinicTiming->deleteClinicTime($request->all(),$id=Auth::user()->id);

      }

}
